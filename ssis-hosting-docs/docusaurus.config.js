module.exports = {
  title: 'COZYROC Cloud Docs',
  tagline: 'The Simplest Way to SSIS in the Cloud',
  url: process.env.REACT_APP_DOCS_URL,
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'volley-labs', // Usually your GitHub org/user name.
  projectName: 'ssis-hosting-docs', // Usually your repo name.
  themeConfig: {
    colorMode: {
      defaultMode: 'light',
      disableSwitch: true,
    },
    footer: {
      style: 'light',
      copyright: 'c'
    },
    gtag: {
      trackingID: 'G-H29NR47VNB',
      anonymizeIP: true
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          routeBasePath: "/",
          sidebarPath: require.resolve('./sidebars.js'),
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    [
      "docusaurus2-dotenv",
      {
        systemvars: true,
      },
    ],
  ]
};