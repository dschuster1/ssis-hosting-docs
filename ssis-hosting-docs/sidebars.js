module.exports = {
  docs: [
    "welcome",
    {
      type: "category",
      label: "Getting Started",
      collapsed: false,
      items: [
        "getting-started/web-app-tour",
        "getting-started/runtimes",
        {
          type: "category",
          label: "On-Premises",
          items: [
            "getting-started/on-premises/why",
            "getting-started/on-premises/agent",
          ]
        },
        "getting-started/ssis+",
        {
          type: "category",
          label: "Plans and Billing",
          items: [
            "getting-started/plans",
            'getting-started/billing',
          ]
        },
        'getting-started/security'
      ],
    },
    {
      type: "category",
      label: "Concepts & Features",
      items: [
        "concepts-features/packages",
        {
          type: "category",
          label: "Package Executions",
          items: [
            "concepts-features/executions/overview",
            {
              type: "category",
              label: "Execution Parameterization",
              items: [
                "concepts-features/executions/parameterization/overview",
                "concepts-features/executions/parameterization/environments",
                "concepts-features/executions/parameterization/rest-tokens",
              ]
            },
            "concepts-features/executions/logs",
            "concepts-features/executions/notifications",
          ]
        },
        {
          type: "category",
          label: "Inbound Webhooks",
          items: [
            "concepts-features/inbound-webhooks/overview",
            "concepts-features/inbound-webhooks/call-webhook-with-request-data",
            "concepts-features/inbound-webhooks/webhook-response"
          ]
        },
        "concepts-features/scheduled-jobs",
        "concepts-features/users",
        "concepts-features/profile"
      ],
    },
    {
      type: "category",
      label: "Getting Help",
      items: [
        "getting-help/support"
      ]
    }
  ],
};
