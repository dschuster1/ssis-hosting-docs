---
title: SSIS Packages
sidebar_label: SSIS Packages
---

[SSIS packages](https://docs.microsoft.com/en-us/sql/integration-services/integration-services-ssis-packages) encode the functionality for an integration scenario by using reusable and customizable building blocks - SSIS Connection managers, Control Flow elements, Data Flow elements, etc.

## Developing SSIS packages

SSIS packages are developed in Visual Studio, where they can be locally tested. When they are ready to be deployed, they can be uploaded in the web app.

**NOTE:** For the shared managed execution environment (from the **Standard plan**), please see the recommendations below: 

* It's recommended that the target for your packages be set to *SQL Server 2019*, as the execution engine uses SQL Server 2019. The automatic upgrade of SSIS packages is not guaranteed to always work properly. 
* It's recommended to use either *Visual Studio 2019* or *Visual Studio 2017*, both of which support SQL Server 2019 as a target.
* It's recommended to use the same version of [COZYROC SSIS+](/getting-started/ssis+) library for developing SSIS packages in Visual Studio, that matches the version used in COZYROC Cloud execution environment. Please also check the [COZYROC SSIS+ to SSDT compatibility matrix](https://desk.cozyroc.com/portal/en/kb/articles/installation-and-getting-started) for other compatibility considerations.

## Uploading SSIS packages

You can reach [the page for uploading a package](https://app.cozyroc.cloud/package/upload) via *Packages > Upload Package* or via the *Dashboard* page. There, you'll need to select the package for upload and, optionally, specify a folder name. If the sensitive data in the package is password protected, it is recommended that you specify the password when uploading the package; otherwise, you'll need to manually input all the sensitive values later.

## Scheduling SSIS package executions

After uploading a package, you can create a [scheduled job](/concepts-features/scheduled-jobs) to run it on a regular basis. You might need to [parameterize the execution](/concepts-features/executions/parameterization/overview) first. We recommend that you do a test run and inspect the logs before scheduling executions.

![img](/img/add-trigger.png)

## Creating package webhook
You can create an [inbound webhook](/concepts-features/inbound-webhooks/overview) and call it outside of COZYROC Cloud application via HTTP GET or HTTP POST request. When you call the webhook url it will trigger package execution.

![img](/img/add-trigger.png)