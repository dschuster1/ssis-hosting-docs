---
title: Scheduled Jobs
sidebar_label: Scheduled Jobs
---

To setup a package for execution (either recurring or one-time) you need to create a scheduled job. You can do this immeditely after uploading a package (via the "Add Trigger" button and then by selecting the "Scheduled Job" option) or later - either from Packages page or from Scheduled Jobs page.

![img](/img/add-trigger.png)

When configuring recurring jobs you have a lot of flexibility when to schedule them. Basically all options available in SQL Server Agent Schedules are also available in COZYROC Cloud Scheduled jobs:
* Configure frequency (daily, weekly or monthly)
* Configure daily frequency (at what time a day or on each what interval)
* Configure the activity period of the scheduled job (from-to date). 

Scheduled jobs can be enabled or disabled (i.e. active or inactive). A one-time job becomes automatically inactive after it gets executed.

## Scheduled Jobs Management

The [Scheduled Jobs page](https://app.cozyroc.cloud/scheduled-job/overview) displays a list of all scheduled jobs (active vs. inactive). You can perform the following operations in this page:
* Sort by job name (Name), package name (Package), agent execution type (Agent), time of last execution (Last executed) and the next time it is scheduled to run (Next Scheduled Time).
* Navigate to a selected scheduled job or the corresponding package
* Filter the grid by package
* Add a new Scheduled job (for an already uploaded package)
* Enable or disable job
