---
title: Manage Your Profile
sidebar_label: Profile Management
---

In your [profile page](https://app.cozyroc.cloud/profile), you can edit your personal information and your preferences:
* First and last name
* Timezone
* Password
* Notifications preferences (more details [here](/concepts-features/executions/notifications))

:::caution
You can also delete entirely your profile data from the "Danger zone" section. Please note, that you can cancel your subscription without deleting your data!
:::
