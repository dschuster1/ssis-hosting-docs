---
title: Call webhook with request data
sidebar_label: Webhook w/ Request Data
---

## Data Sources

Any package parameter of type `String` can be configured to get its value from the webhook request data. There are 3 request data sources:

### Body

The whole request body will be converted to string and set as parameter value. A body with one of the following content types: `application/json`, `application/xml` and `text/plain` will be applied directly as it is received. In case a different content type is detected, it will be converted to Base64 encoded string and used this way. 

:::note
* The request body size should be **less than or equal to 2 MB** (exceeding the limit will result in **413 Request Entity Too Large** response).
* The request should be **HTTP POST**.
:::

### Headers

All request headers will be collected and converted to JSON key-value format.

For example request headers

```
Host: cozyroc.com
Origin: https://cozyroc.com
Referer: https://cozyroc.com/
```

will be converted to

```
{
    "Host": ["cozyroc.com"],
    "Origin": ["https://cozyroc.com"],
    "Referer": ["https://cozyroc.com/"]
}
```

### Query String

The request query string will be converted to JSON key-value format.

For example request query string

```
?user=john&code=68a0711871ab41e89f35d6e6e3a9f8b1
```

will be converted to

```
{
    "user": ["john"],
    "code": ["68a0711871ab41e89f35d6e6e3a9f8b1"]
}
```

## Parameterization

1. While developing a package create one or more parameters that will hold the webhook request data.

![img](/img/inbound-webhooks/package-webhook-param-body.png)

2. Upload your package and add a webhook trigger.

![img](/img/add-trigger.png)

3. Your parameters will be available in the **Parameters** section of the webhook page.

![img](/img/inbound-webhooks/package-webhook-param-overview.png)

4. Configure the webhook data source for each of your parameters. 

![img](/img/inbound-webhooks/parameterize-webhook.png)

5. After the configuration is done, your parameters should look like this:

![img](/img/inbound-webhooks/package-webhook-param-overview-assigned.png)

6. Click **Create** to save your webhook configuration.


## Testing Webhook

1. Copy Url from the webhook page.

![img](/img/inbound-webhooks/copy-webhook-url.png)

2. Open your favorite API client (e.g. Postman, Insomnia or Visual Studio Code with Thunder Client extension). In this guide we are going to use **Postman**.

3. Choose the HTTP verb. We currently support `HTTP GET` and `HTTP POST` for all webhook endpoints. Note that request body is available only for **POST** requests. 

4. Edit your request data.

![img](/img/inbound-webhooks/override-webhook-param.png)

5. If the request is successful you should receive `200 OK` as a response. If the webhook is disabled  you will receive `403 Forbidden` as a response.

6. After the package execution is completed you can verify the execution parameters on execution details page.

![execution-result](/img/inbound-webhooks/execution-result.png)









