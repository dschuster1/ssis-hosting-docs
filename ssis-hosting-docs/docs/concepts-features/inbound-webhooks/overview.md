---
title: Inbound Webhooks Overview
sidebar_label: Overview
---
The inbound webhooks feature allows **external systems** to trigger package executions, enabling you to implement **event driven workflows** in COZYROC Cloud. To setup a package for execution via inbound webhook you need to create an inbound webhook. You can do this from any package page (via the "Add Trigger" button and then by selecting the "Webhook" option) or using the webhooks management pages.

![img](/img/add-trigger.png)

After creation inbound webhooks are enabled by default. If you want to prevent a webhook from being triggered, you can disable it at any time from its own page or from the related package page.

## Inbound Webhooks Management

The [Inbound Webhooks page](https://app.cozyroc.cloud/webhooks/inbound/overview) displays a list of all inbound webhooks (enabled and disabled) and allows you to perform a number of actions:
* Add a new webhook or delete an existing one
* Copy webhook url
* Enable or disable webhooks

## Trigger package execution via Webhook

You can trigger a webhook by copying the webhook url from its details page and then making a `HTTP GET` or `HTTP POST` request to that url.

![img](/img/inbound-webhooks/copy-webhook-url.png)

* `HTTP GET` - supports reading data from request's **query string** and **headers**
* `HTTP POST` - supports reading data from request's **query string**, **headers** and **body**

## Using external event data in you package

The webhook page allows to set package parameters based on the event data sent with webhook request. Currently we support 3 types of data sources:
* `Body` - gets data from request body (only for `HTTP POST` requests) as text
* `HTTP Headers` - gets data from request headers and serializes it to JSON in a key-value format
* `Query` - gets data from query string and serializes it to JSON in a key-value format

![img](/img/inbound-webhooks/assign-webhook-data.png)

Learn more about the topic [here](/concepts-features/inbound-webhooks/call-webhook-with-request-data).

## Response configuration

Some external systems will require a response from the webhook (e.g. as part of a verification phase). From the webhook page you are able to configure the **response** details - choose a `content type` and use a simple template syntax to build the **response body**. All `event data sources` are also available here (Query, Headers and Body). 

![img](/img/inbound-webhooks/custom-response.png)

Learn more about the topic [here](/concepts-features/inbound-webhooks/webhook-response).

## Notifications

Similar to Scheduled jobs you can set the Webhook to send email notifications to organization users on package execution success and error. 