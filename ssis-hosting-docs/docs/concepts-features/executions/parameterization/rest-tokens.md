---
title: REST Tokens
sidebar_label: REST Tokens
---

On the Parameterization, there is a sub-section called **REST Tokens**. Use this page to upload any REST token files that were created for the packages you plan to execute. Those REST tokens are generated during design-time when you configure a corresponding COZYROC SSIS+ [REST Connection Manager](http://www.cozyroc.com/ssis/rest-connection).

From the REST Tokens page, you can perform the following actions:

* Upload a Token file
* Change the name of a Token file
* Change the Token file associated with an existing Token file name on the COZYROC Cloud
* Download a Token file
* Delete a Token file

Once a Token file has been uploaded, the corresponding REST Connection Manager `TokenFile` property needs to be configured to refer to the uploaded REST Token file. This can be done by opening the package or the scheduled job or the inbound webhook in the COZYROC Cloud app and editing the *TokenFile* parameter of the corresponding Connection Manager to select the corresponding REST Token file that was uploaded.