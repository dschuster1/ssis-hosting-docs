---
title: Environments
sidebar_label: Environments
---

Environments are an advanced parameterization technique. They allow you to set up a centralized storage of parameterization parameters via **environment variables**. Thus, multiple scheduled jobs can refer to a single variable and any required customization (e.g. a change of resource URL) can be done in a single place, automatically propagating the change to all related executions.

:::note
COZYROC Cloud Environments are analogous to environments in the SSIS Catalog.
:::

You can have create any number of environments and define any number of variables in it. An environment variable has the following properties:
* Name
* Description
* Sensitive flag
* Data type
* Value

A scheduled job or an inbound webhook can be linked to a single environment. During runtime, the value of any referenced variable gets resolved from the corresponding environment. If the variable is missing, the execution will fail.

:::tip
If you set up environments like "Production" and "Staging", once you prepare a scheduled job or an inbound webhook which works using the "Staging" environment variables, you can clone it, change the environment to "Production" and if it has the same set of variables correctly set up (i.e. variables with the same names), the job or the webhook should work properly for production.
:::
