---
title: Execution Parameterization
sidebar_label: Parameterization Overview
---

Any SSIS package that you upload on the COZYROC Cloud can be a template for multiple scheduled jobs or inbound webhooks, as it can be further parameterized. For example, if you need to use different connection strings for your staging and production environments, instead of having to upload two very similar versions of a package, you only need to parameterize a single package in two different ways.

## What's parameterizable?

### Package Parameters
Package parameters allow you to easily modify package execution without having to edit and redeploy the package. The parameters must be set up when the package is designed. Then, when the package is uploaded to the COZYROC Cloud, the parameters will show up under “Parameters” when the package is opened.

### Connection Manager Properties
All connection manager properties can be modified once a package has been uploaded to the COZYROC Cloud. The properties will initially have the values configured at design time. In addition, if no password is specified when the package is uploaded, then any sensitive information will need to be entered manually in the connection managers properties (in the package, in the scheduled job(s) or in the inbound webhook(s)).

### Property Overrides
Even if you haven't introduced a package parameter for easy customization of a particular property, you will still be able to modify any property value via a property override. You just need to specify the full path of the property.

## How to parameterize values?
There is a standard popup screen **"Edit property"** for parameterizing any property. There you can either specify a value directly or choose *"use environment"* and select a variable from the list of the environments linked to a job. See more info about [enviroments](/concepts-features/executions/parameterization/environments).

**NOTE:** The `TokenFile` property of the COZYROC SSIS+ [REST Connection Manager](http://www.cozyroc.com/ssis/rest-connection) requires a custom setup (see [here](/concepts-features/executions/parameterization/rest-tokens)). A dropdown with a list of tokens appears when you paremeterize this property.
