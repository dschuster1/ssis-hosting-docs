---
title: Execution Logs
sidebar_label: Execution Logs
---

Once a package execution is started you can monitor its progress in real time. The logs for completed executions are accessible for 14 days. You can also download the log file in JSON format.

### Logging Levels

* **None** - no log entries from the SSIS runtime will be recorded (only system logs)
* **Basic** - basic SSIS logging (that's the recommended logging level)
* **Verbose** - verbose SSIS logs (**NOTE:** Use only for troubleshooting purposes)
* **Original** - use logging levels specified in the SSIS package (in the package you can configure custom logging settings for different elements of the package)