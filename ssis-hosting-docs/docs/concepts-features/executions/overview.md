---
title: Executions Overview
sidebar_label: Executions Overview
---

**NOTE**: We'll refer to *SSIS package execution* as *execution* for brevity.

## Scheduled vs Inbound Webhooks vs On-Demand Executions

An execution can be triggered by scheduled job, inbound webhook or on-demand. On-demand executions are used mostly during the initial testing phase for the package. Once the package parameterization is verified to be working, you will probably set up the package to execute on a recurring basis. It is also possible to schedule a package to execute only once at a particular time.

When scheduling a package to execute on a recurring basis, the first thing to check is that the time zone is set correctly. Then you will select the frequency with which you want the job to run. You have a choice of daily, weekly or monthly. There are an abundance of ways to set up your recurring executions, ranging from having the job run one time each day to running once per month. There is also the option to have a job run multiple times per day.

Or you can create an inbound webhook which you can call outside of COZYROC Cloud application via HTTP request.

## Execution Statuses

At any given time, a package execution can be in one of the following statuses:

* **Pending** - package is preparing for execution
* **Running** - package is currently being executed by the SSIS package runner.
* **Succeeded** - package execution has completed normally (i.e. `DTSExecResult.Success`)
* **Failed** - package execution has completed with failure (i.e. `DTSExecResult.Failure`)
* **Timed Out** - package execution didn't complete within the maximum allowed time (by default 30 minutes)
* **Aborted** - internal error with package execution (e.g. hasn't started or hasn't completed normally)

## General Tips

### Working with temporary files

In general, the sources and destinations used in your packages should be cloud-based or accessible via the Internet. You will have access to 1GB of temporary storage space on the COZYROC Cloud execution server, so it should be used sparingly. There is no guarantee that files you write on the server will be available for a prolonged period of time. Any files of interest that are created during package execution should be moved off the server ASAP and that's best done from within the package that generated them. It may also be possible to access them and move them using another package, as long as that package is run soon after the package that created them completes.

## Further Reading
* How to [parameterize executions](/concepts-features/executions/parameterization/overview)
* How to setup [e-mail notifications](/concepts-features/executions/notifications)
* How to configure and analyze [execution logs](/concepts-features/executions/logs)
