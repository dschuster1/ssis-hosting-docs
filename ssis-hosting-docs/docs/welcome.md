---
id: welcome
title: Welcome to COZYROC Cloud
sidebar_label: Welcome
slug: /
---

[COZYROC Cloud](http://www.cozyroc.cloud) offers a cloud-based orchestration of data and application integration scenarios, thus allowing you to easily manage your workflows from a browser.

The service is powered by [SQL Server Integration Services](https://docs.microsoft.com/en-us/sql/integration-services/sql-server-integration-services) (the renowned enterprise-grade ETL platform by Microsoft). You can easily upload SSIS packages, schedule their execution, automatically run packages by subscribing to external events via inbound webhooks, monitor the execution progress and get notified about problems.

Besides orchestration, [COZYROC Cloud](http://www.cozyroc.cloud) can also be an end-to-end [iPaaS](https://en.wikipedia.org/wiki/Cloud-based_integration) solution, by providing a [managed SSIS execution environment](/getting-started/runtimes), effectively eliminating the hassle of hardware and software infrastructure maintenance.
## Getting started

To get started, please check out the following topics:
* [Quick Tour of the Web Application](/getting-started/web-app-tour)
* [Runtime Environments](/getting-started/runtimes)
* [Plans](/getting-started/plans) and [Billing](/getting-started/billing)
* [Security & Data Privacy](/getting-started/security)

**NOTE:** Upon sign-up for your trial you will have to choose which of the [two plans](/getting-started/plans) you are interested in using.
