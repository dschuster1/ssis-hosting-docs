---
title: Why Consider On-Premises Plan?
sidebar_label: Use Cases
---


At the moment the main reason to consider subcribing to a COZYROC Cloud **On-Premises** plan is if your organization employs multiple SSIS execution environments and you want to make them easier to manage. 

After installing a [COZYROC Cloud Agent](/getting-started/on-premises/agent) on each of your servers, you'll be able to manage these environments in a web-based control center. Also, if one environment is down (e.g. for maintenance), the COZYROC Cloud will automatically redirect scheduled executions to another environment, if available.

**NEW FEATURE:** On-premises runtime is able to participate in hybrid event-driven scenarios. E.g. you can setup what webhooks to consume from third-party apps and each [inbound webhook](/concepts-features/inbound-webhooks/overview) will trigger a package execution on your on-premises environment.
