---
title: Subscription Plans
sidebar_label: Plans
---

[COZYROC Cloud](https://cozyroc.cloud) service comes with two plans, each with a **yearly** or a **monthly** commitment.
### On-Premises plan

The **On-Premises** plan is limited to running SSIS packages only on your own self-hosted SSIS execution environment(s). It supports:
* Web-based adminstration and monitoring of SSIS executions
* Unlimited [SSIS packages](/concepts-features/packages) & [scheduled jobs](/concepts-features/scheduled-jobs) & [inbound webhooks](/concepts-features/inbound-webhooks/overview)
* Unlimited [collaborators](/concepts-features/users) on your team
* 15 days of execution log retention

### Standard plan

The **Standard** plan includes everything offered in the **On-Premises** plan, and in addition provides a managed (by COZYROC) SSIS execution environment, where [COZYROC SSIS+](/getting-started/ssis+) addon tasks and components are readily available. The managed environment processing resources are shared, but tenants' data is isolated.

The restrictions of the managed SSIS execution environment in the **Standard** plan are:
* 100 execution hours/month
* Single job duration up to 30 minutes
* Free 10GB/month of network traffic. Extra traffic charged $0.25 per 1GB
* 1GB storage for temporary execution files

If your company has special needs, please contact us at sales@cozyroc.cloud for a custom quote.

:::note
The **Standard** plan of COZYROC Cloud service works with a hosted MS SQL Server 2019 Standard Edition. Please note that SSIS comes with Microsoft SQL Server and it is not offered separately. As COZYROC is a Microsoft SPLA partner, according to your SPLA SQL Server license, you will also get an access to any SQL Server features besides SSIS.
:::
