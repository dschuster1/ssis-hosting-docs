---
title: Runtime Environments
sidebar_label: Runtime Environments
---

An SSIS package that's uploaded in COZYROC Cloud can be executed in the following environments:
* a managed SSIS execution server (available only in the [Standard plan](/getting-started/plans)) 
* an on-premises SSIS execution server.

Please find below a comparison chart of the two types of execution environments:

|              | Managed runtime | On-premises runtime |
| -----------  | ----------- | ----------- |
| Maintained by | COZYROC | Client |
| Benefits | No-hassle, cost-effective, shared runtime. | Full-control over what's installed. Direct access to on-premises resources. |
| Available in | [Standard plan](/getting-started/plans)| [All plans](/getting-started/plans) |
| SSIS Licensing | Via SPLA service provider * | Client's responsibility |
| Target Version | SQL Server/SSIS 2019 | Any SSIS versions that are installed on the client's premises |
| Addons | Only [COZYROC SSIS+](/getting-started/ssis+) | Any third-party SSIS components or custom DLLs can be installed |
| COZYROC SSIS+ | Included in the [Standard plan](/getting-started/plans) | Purchase [separately](https://cozyroc.com/purchase) |
| Prerequisites | none | Installation of an [agent](/getting-started/on-premises/agent) |


\* The SQL Server Integration Services is licensed with the help of a SPLA service provider of Microsoft SQL Server hosting services. COZYROC LLC is a certified SPLA service provider.
