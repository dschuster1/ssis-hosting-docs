---
title: Billing
sidebar_label: Billing
---

When you sign up for a COZYROC Cloud account, you can choose a **monthly** or an **annual** subscription (you get 20% off with an annual subscription). 

### Free trial

Your 14-day trial starts upon confirming your e-mail address. While your trial is active, you can use all of the features of the application. When you are ready to become a paid customer, go to the [Subscription page](https://app.cozyroc.cloud/subscription) and enter your payment details. After your trial expires, your data will still be accessible, but you won't be able to execute packages (neither scheduled, nor on-demand, nor via webhook).

### Invoices and payments

The billing and payment collection is handled by [Stripe](https://stripe.com/) and [Zoho Subcriptions](https://www.zoho.com/subscriptions/). From your [Subscription page](https://app.cozyroc.cloud/subscription), you will have access to a [Zoho Subcriptions customer portal](https://www.zoho.com/subscriptions/help/customer-portal-functions.html), from where you can view and download information about your invoices and payments.