---
title: Security & Data Privacy
sidebar_label: Security & Data Privacy
---

To protect the data in our customers' accounts, we employ the following security best practices:
* All sensitive data is encrypted in our databases with 256-bit AES encryption
* All network communication is secured with TLS 1.2
* Strict access controls and procedures are implemented.

Please also refer to [COZYROC Cloud Privacy Policy](https://cozyroc.cloud/Privacy)