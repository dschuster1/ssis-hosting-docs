import React from "react";
import "./YouTubePlayer.css";

const YouTubePlayer = ({ embedUrl }) => {
  return (
    <>
      <div className="YouTubePlayer">
        <iframe
          width="720"
          height="405"
          src={embedUrl}
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
      </div>
    </>
  );
};

export default YouTubePlayer;
