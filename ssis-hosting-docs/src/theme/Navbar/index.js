import React, { useState } from 'react';
import "./styles.css";

// Link item
// {
//   href: process.env.REACT_APP_FRONTPAGE_URL,
//   label: "Cloud"
// }
const NavLink = ({ href, label }) => {
  return (
    <li className="nav-item">
      <a href={href} className="font-weight-bold">{label}</a>
    </li>
  );
};

const navLinks = [];

function Navbar() {
  const [openMobileMenu, setOpenMobileMenu] = useState(false);

  const activeClass = openMobileMenu ? 'active' : '';
  const toggleMobileMenu = () => setOpenMobileMenu((open) => !open);

  return (
    <nav className="navbar">
      <div className="navbar-bg"></div>
      <div className="flex justify-space-between navbar-container">
        <a href="/" className="navbar-brand">
          <img src="/img/logo_bg.svg" alt="logo" className="logo" />
          <span className="name">COZYROC</span>
        </a>
        {navLinks.length > 0 && (
          <div className={`flex navbar-links-wrap ${activeClass}`}>
            <ul className="flex navbar-links-left navbar-links">
              {navLinks.map((item, i) => (<NavLink key={i} {...item} />))}
            </ul>
          </div>
        )}

        <div className="flex navbar-buttons">
          <a href={process.env.REACT_APP_MANAGEMENT_URL} className="font-weight-bold sign-in-link">Sign in</a>
          <div className="sign-up">
            <a href={`${process.env.REACT_APP_MANAGEMENT_URL}/auth/sign-up`} className="btn font-weight-bold atlas-cta cta-orange">Sign up</a>
          </div>
          {navLinks.length > 0 && (
            <div className={`hamburger ${activeClass}`} onClick={toggleMobileMenu}>
              <span className="bar"></span>
              <span className="bar"></span>
              <span className="bar"></span>
            </div>
          )}
        </div>
      </div>

      <div className="collapse navbar-collapse justify-content-end" id="navbarNav">

      </div>
    </nav>
  );
}

export default Navbar;